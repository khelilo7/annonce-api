# Annonce-api

Annonce-api is an API that handles annonces, this project was made in an interview process.

## Start Application

To launche the application you either have to hard code the envirement variables and have a local postgres lanched on your machine on port 5432 and excute the schema script 

Or take the easy road and just install docker.

Launching application with docker 

First you have to build the docker image 
```bash
cd annonce-api
docker build . -t annonce-api 
```
Then you just have to
```bash
docker-compose up -d
```
to check your application is up and runnig you can use
```bash 
docker logs [container]
```
to shutdown the application 
```bash
docker-compose down
```

## Test
First you have to run the docker postgres image, everything is already configured inside the docker-compose
```bash
cd test
docker-compose up -d
```
Then you just have to wait for the database to be created you can check that with
```bash 
docker logs [container]
```
Once the database is created  
```bash
cd ..
go test -v ./... --coverprofile cover.out
```

to shutdown the postgres container 
```bash
docker-compose down
```

## API

The REST API to the annonce app is described below.

## Get list of annonces

### Request

`GET /annonce/`

    curl -i -H 'Accept: application/json' http://localhost:8080/annonce/
#### filters
- query : search with keyword
- marue : use marque to filter annonces

### Response

    HTTP/1.1 200 OK
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Content-Length: 2

    [list of annonces]

## Create a new annonce

### Request

`POST /annonce/`

    curl -i -H 'Accept: application/json' -d 'titire=Foo&contenu=new' http://localhost:8080/annonce

### Response

    HTTP/1.1 201 Created
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 201 Created
    Connection: close
    Content-Type: application/json
    Location: /annonce/1
    Content-Length: 36

    {"id":1,"titre":"Foo","contenu":"new"}

## Get a specific annonce

### Request

`GET /annonce/id`

    curl -i -H 'Accept: application/json' http://localhost:8080/annonce/1

### Response

    HTTP/1.1 200 OK
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Content-Length: 36

    {"id":1,"titre":"Foo","contenu":"new"}

## Delete a specific annonce

### Request

`Delete /annonce/id`

    curl -X DELETE -i -H 'Accept: application/json' http://localhost:8080/annonce/1

### Response

    HTTP/1.1 200 OK
    Date: Thu, 24 Feb 2011 12:36:30 GMT
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Content-Length: 36

    {"status": "annonce deleted"}

## Database

For this application we're using a postgres database.

## Tables
### Annonce
- id  BIGSERIAL PRIMARY KEY NOT NULL,
- creation_date TIMESTAMP WITH TIME ZONE  DEFAULT NOW() NOT NULL,
- last_update TIMESTAMP WITH TIME ZONE  DEFAULT NOW() NOT NULL,
- titre   VARCHAR(50)   DEFAULT 'empty' NOT NULL,
- contenu   VARCHAR   DEFAULT 'empty' NOT NULL,
- marque   VARCHAR(10)   DEFAULT 'empty',
- model   VARCHAR(10)   DEFAULT 'empty'

# Author
Khalil JRIDI.