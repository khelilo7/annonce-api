DROP DATABASE IF EXISTS annonce_db;

CREATE DATABASE annonce_db;

\c annonce_db

CREATE TABLE annonce (
    id  BIGSERIAL PRIMARY KEY NOT NULL,
    creation_date TIMESTAMP WITH TIME ZONE  DEFAULT NOW() NOT NULL,
    last_update TIMESTAMP WITH TIME ZONE  DEFAULT NOW() NOT NULL,
    titre   VARCHAR(50)   DEFAULT 'empty' NOT NULL,
    contenu   VARCHAR   DEFAULT 'empty' NOT NULL,
    marque   VARCHAR(10)   DEFAULT 'empty',
    model   VARCHAR(10)   DEFAULT 'empty'
);

CREATE INDEX ON annonce(titre);
CREATE INDEX ON annonce(marque);