FROM golang:1.17-alpine

WORKDIR /annonce/

COPY . ./

RUN go mod download

RUN go build -o ./cmd/main ./cmd/main.go 

ENTRYPOINT [ "./cmd/main" ]

ENV DB_NAME="annonce_db"
ENV PORT=8081
ENV DB_USER="postgres"
ENV DB_PASS="123456"
ENV DB_PORT=5432
ENV DB_HOST='db'