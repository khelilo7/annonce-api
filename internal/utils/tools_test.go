package utils_test

import (
	"testing"

	"github.com/maxatome/go-testdeep/td"

	"annonce/internal/utils"
)

func TestGetEnv(t *testing.T) {
	td.Cmp(t, utils.Getenv("something", "test"), "test")
}

func TestContains(t *testing.T) {
	td.Cmp(t, utils.Contains([]string{"ab", "cd"}, "cd"), true)
	td.Cmp(t, utils.Contains([]string{"ab", "cd"}, "ef"), false)
}
