package utils

func ValidateMarque(marque string) bool {
	if marque == "" {
		return true
	}

	for _, v := range MarqueByModel {
		if Contains(v, marque) {
			return true
		}
	}

	return false
}

func ValidateModel(model string) bool {
	if model == "" {
		return true
	}

	for k, _ := range MarqueByModel {
		if k == model {
			return true
		}
	}

	return false
}
