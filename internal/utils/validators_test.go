package utils_test

import (
	"testing"

	"github.com/maxatome/go-testdeep/td"

	"annonce/internal/utils"
)

func TestValidateMarque(t *testing.T) {
	td.Cmp(t, utils.ValidateMarque("M5"), true)
	td.Cmp(t, utils.ValidateMarque("Y9"), false)
}

func TestValidateodel(t *testing.T) {
	td.Cmp(t, utils.ValidateModel("BMW"), true)
	td.Cmp(t, utils.ValidateModel("BMX"), false)
}
