package utils

import (
	"regexp"
	"strings"
)

func FilterMarque(s string) string {
	if s == "" {
		return ""
	}

	marque := ""
	for _, marques := range MarqueByModel {
		for _, m := range marques {
			m1 := regexp.MustCompile(`(\d)`)
			m2 := m1.ReplaceAllString(strings.ToLower(m), `\s*$1`)
			re := regexp.MustCompile(`\b` + m2 + `\b`)
			if re.FindString(strings.ToLower(s)) != "" {
				marque = m
			}
		}
	}
	return marque
}
