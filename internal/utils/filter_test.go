package utils_test

import (
	"testing"

	"github.com/maxatome/go-testdeep/td"

	"annonce/internal/utils"
)

func TestFilterMarque(t *testing.T) {
	td.Cmp(t, utils.FilterMarque("rs4 avant"), "Rs4")
	td.Cmp(t, utils.FilterMarque("gran turismo serie 5"), "Serie 5")
	td.Cmp(t, utils.FilterMarque("ds 3 crossback"), "Ds3")
	td.Cmp(t, utils.FilterMarque(""), "")
}
