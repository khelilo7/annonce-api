package testutils

import (
	"annonce/internal/dbmodels"
	"testing"
)

func CreateAnnonce(t *testing.T, titre, contenu, marque, model string) dbmodels.Annonce {
	annonce := dbmodels.Annonce{
		Titre:   titre,
		Contenu: contenu,
		Marque:  marque,
		Model:   model,
	}

	db := dbmodels.NewDBConn()
	_, err := db.Model(&annonce).Insert()
	if err != nil {
		t.Helper()
		t.Fatal("failed to insert annonce")
	}

	t.Cleanup(func() {
		_, err = db.Model(&annonce).WherePK().Delete()
		if err != nil {
			t.Fatalf("failed to delete annonce %d", annonce.ID)
		}
	})

	return annonce
}
