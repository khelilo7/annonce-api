package testutils

import (
	api "annonce/handlers"
	"fmt"

	"github.com/gin-gonic/gin"
)

func GetRouter() *gin.Engine {
	app := new(api.App)
	err := app.Init()
	if err != nil {
		fmt.Println(err)
	}
	return app.Router
}
