package dbmodels

import "time"

type Annonce struct {
	tableName struct{} `pg:"annonce"`

	ID           int64      `pg:"id" json:"id"`
	Titre        string     `pg:"titre,notnull,use_zero" json:"titre"`
	Contenu      string     `pg:"contenu,notnull,use_zero" json:"contenu"`
	Marque       string     `pg:"marque" json:"marque,omitempty"`
	Model        string     `pg:"model" json:"model,omitempty"`
	CreationDate *time.Time `pg:"creation_date" json:"-"`
	LastUpdate   *time.Time `pg:"last_update" json:"-"`
}
