package dbmodels_test

import (
	"testing"

	"github.com/maxatome/go-testdeep/td"

	"annonce/internal/dbmodels"
)

func TestNewDBConn(t *testing.T) {
	td.CmpNotNil(t, dbmodels.NewDBConn())
}
