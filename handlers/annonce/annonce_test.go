package annonce_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/maxatome/go-testdeep/helpers/tdhttp"
	"github.com/maxatome/go-testdeep/td"

	"annonce/internal/testutils"
)

func TestGetListAnnonce(t *testing.T) {
	ta := tdhttp.NewTestAPI(t, testutils.GetRouter())

	ta.Name("test get list annonce - no result").
		Get("/annonce").
		CmpStatus(http.StatusOK).
		CmpJSONBody(td.JSON(`{"status": "No result found"}`))

	for i := 0; i < 5; i++ {
		_ = testutils.CreateAnnonce(t, fmt.Sprintf("annonce%d", i), "content", "", "")
	}

	ta.Name("test get list annonce - no filter").
		Get("/annonce").
		CmpStatus(http.StatusOK).
		CmpJSONBody(td.Len(5))

	_ = testutils.CreateAnnonce(t, "voiture1", "content", "M535", "BMW")
	_ = testutils.CreateAnnonce(t, "voiture1", "content", "M535", "BMW")
	ta.Name("test get list annonce - marque filter").
		Get("/annonce?marque=M535").
		CmpStatus(http.StatusOK).
		CmpJSONBody(td.Len(2))

	_ = testutils.CreateAnnonce(t, "voit", "content", "Rs4", "audi")
	ta.Name("test get list annonce - with query").
		Get("/annonce?query=voi").
		CmpStatus(http.StatusOK).
		CmpJSONBody(td.Len(3))

	ta.Name("test get list annonce - with query and marque").
		Get("/annonce?query=voi&&marque=Rs4").
		CmpStatus(http.StatusOK).
		CmpJSONBody(td.Len(1))
}

func TestGetAnnonce(t *testing.T) {
	ta := tdhttp.NewTestAPI(t, testutils.GetRouter())

	ta.Name("test get annonce - bad input").
		Get("/annonce/sd").
		CmpStatus(http.StatusInternalServerError).
		CmpJSONBody(td.JSON(`{"status": "Server error"}`))

	ta.Name("test get annonce - non existant annonce").
		Get("/annonce/1").
		CmpStatus(http.StatusNotFound).
		CmpJSONBody(td.JSON(`{"status": "No result found"}`))

	annonce := testutils.CreateAnnonce(t, "annonce", "content", "", "")

	ta.Name("test get annonce").
		Get(fmt.Sprintf("/annonce/%d", annonce.ID)).
		CmpStatus(http.StatusOK).
		CmpJSONBody(td.JSON(`
	{
		"id": $1,
		"titre": "annonce",
		"contenu": "content",
		"model": "empty",
		"marque": "empty"
	}`,
			td.NotZero()),
		)
}

func TestCreateAnnoce(t *testing.T) {
	var id float64
	ta := tdhttp.NewTestAPI(t, testutils.GetRouter())

	ta.Name("test Create annonce").
		PostJSON("/annonce",
			json.RawMessage(`{"titre": "annonce_created", "contenu": "conten"}`)).
		CmpStatus(http.StatusOK).
		CmpJSONBody(td.SuperJSONOf(`
		{
			"id": $id, 
			"titre": "annonce_created", 
			"contenu": "conten"
		}`,
			td.Tag("id", td.Catch(&id, td.NotZero()))),
		)

	ta.Name("test Create annonce - bad model").
		PostJSON("/annonce",
			json.RawMessage(`{"titre": "annonce_created", "contenu": "conten", "model": "m"}`)).
		CmpStatus(http.StatusBadRequest).
		CmpJSONBody(td.JSON(`{"error": "Model sould be one of the selected list of models"}`))

	ta.Name("test Create annonce - bad marque").
		PostJSON("/annonce",
			json.RawMessage(`{"titre": "annonce_created", "contenu": "conten", "model": "BMW", "marque": "m"}`)).
		CmpStatus(http.StatusBadRequest).
		CmpJSONBody(td.JSON(`{"error": "Marque sould be one of the selected list of marques"}`))

	ta.Name("test delete annonce").
		DeleteJSON(fmt.Sprintf("/annonce/%d", int(id)), json.RawMessage("{}")).
		CmpStatus(http.StatusOK).
		CmpJSONBody(td.JSON(`{"status": "annonce deleted"}`))

}

func TestDeleteAnnoce(t *testing.T) {
	ta := tdhttp.NewTestAPI(t, testutils.GetRouter())

	ta.Name("test get annonce - bad input").
		DeleteJSON("/annonce/sd", json.RawMessage("{}")).
		CmpStatus(http.StatusInternalServerError)
	annonce := testutils.CreateAnnonce(t, "annonce", "content", "", "")

	ta.Name("test delete annonce").
		DeleteJSON(fmt.Sprintf("/annonce/%d", annonce.ID), json.RawMessage("{}")).
		CmpStatus(http.StatusOK).
		CmpJSONBody(td.JSON(`{"status": "annonce deleted"}`))

	ta.Name("test get annonce").
		Get(fmt.Sprintf("/annonce/%d", annonce.ID)).
		CmpStatus(http.StatusNotFound).
		CmpJSONBody(td.JSON(`{"status": "No result found"}`))
}
