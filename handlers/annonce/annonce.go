package annonce

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg/v10"
	log "github.com/sirupsen/logrus"

	"annonce/internal/dbmodels"
	"annonce/internal/utils"
)

func GetAnnonceList(c *gin.Context) {
	log.Info("Hitting Get Annonce List")

	db := dbmodels.NewDBConn()
	var annonces []dbmodels.Annonce

	query := c.Query("query")
	marque := c.Query("marque")
	if query != "" && marque != "" {
		marque = utils.FilterMarque(marque)
		err := db.Model(&annonces).Where("marque = ?", marque).Where("titre LIKE ?", "%"+query+"%").Select()
		if err != nil {
			log.Errorf("Could not get annonces from Db, err %v", err)
			c.JSON(http.StatusInternalServerError, gin.H{"status": "Could not get annonces from Db"})
			return
		}
	} else if marque != "" {
		marque = utils.FilterMarque(marque)
		err := db.Model(&annonces).Where("marque = ?", marque).Select()
		if err != nil {
			log.Errorf("Could not get annonces from Db, err %v", err)
			c.JSON(http.StatusInternalServerError, gin.H{"status": "Could not get annonces from Db"})
			return
		}
	} else if query != "" {
		err := db.Model(&annonces).Where("titre LIKE ?", "%"+query+"%").Select()
		if err != nil {
			log.Errorf("Could not get annonces from Db, err %v", err)
			c.JSON(http.StatusInternalServerError, gin.H{"status": "Could not get annonces from Db"})
			return
		}
	} else {
		// Select all annonces.
		err := db.Model(&annonces).Select()
		if err != nil {
			log.Errorf("Could not get annonces from Db, err %v", err)
			c.JSON(http.StatusInternalServerError, gin.H{"status": "Could not get annonces from Db"})
			return
		}
	}

	if len(annonces) == 0 {
		c.JSON(http.StatusOK, gin.H{"status": "No result found"})
		return
	}

	jsonData, err := json.Marshal(&annonces)
	if err != nil {
		log.Errorf("Error while parsing to json, err %v", err)
		c.JSON(http.StatusOK, gin.H{"status": "No result found"})
		return
	}

	c.Data(http.StatusOK, "application/json", jsonData)
}

func GetAnnonce(c *gin.Context) {
	log.Info("Hitting Get Annonce")

	ID := c.Params.ByName("id")
	intID, err := strconv.Atoi(ID)
	if err != nil {
		log.Errorf("could not convert %s to int, err %v", ID, err)
		c.JSON(http.StatusInternalServerError, gin.H{"status": "Server error"})
		return
	}

	db := dbmodels.NewDBConn()
	annonce := &dbmodels.Annonce{ID: int64(intID)}
	err = db.Model(annonce).WherePK().Select()
	if err != nil {
		if err == pg.ErrNoRows {
			log.Error("annonce not found")
			c.JSON(http.StatusNotFound, gin.H{"status": "No result found"})
			return
		}
		log.Errorf("Could not get annonce from Db, err %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"status": "Could not get annonce from Db"})
		return
	}

	jsonData, err := json.Marshal(annonce)
	if err != nil {
		log.Errorf("Error while parsing to json, err %v", err)
		c.JSON(http.StatusOK, gin.H{"status": "No result found"})
		return
	}

	if jsonData == nil {
		c.JSON(http.StatusNotFound, gin.H{"status": "No result found"})
		return
	}

	c.Data(http.StatusOK, "application/json", jsonData)
}

func CreateAnnonce(c *gin.Context) {
	log.Info("Hitting Create Annonce")

	var annonce dbmodels.Annonce
	c.ShouldBindJSON(&annonce)

	if !utils.ValidateModel(annonce.Model) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Model sould be one of the selected list of models"})
		return
	}

	if !utils.ValidateMarque(annonce.Marque) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Marque sould be one of the selected list of marques"})
		return
	}

	db := dbmodels.NewDBConn()
	_, err := db.Model(&annonce).Where("titre = ?", annonce.Titre).SelectOrInsert()
	if err != nil {
		log.Errorf("Could not create new annonce, err %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"status": "Could not create new annonce"})
		return
	}

	c.JSON(http.StatusOK, annonce)
}

func DeleteAnnonce(c *gin.Context) {
	log.Info("Hitting Delete Annoce")

	ID := c.Params.ByName("id")
	intID, err := strconv.Atoi(ID)
	if err != nil {
		log.Errorf("could not convert %s to int, err %v", ID, err)
		c.JSON(http.StatusInternalServerError, gin.H{"status": "Server error"})
		return
	}

	annonce := dbmodels.Annonce{ID: int64(intID)}
	db := dbmodels.NewDBConn()
	_, err = db.Model(&annonce).WherePK().Delete()
	if err != nil {
		log.Errorf("Could not delete annonce from Db, err %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"status": "Could not delete annonce from Db"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "annonce deleted"})
}
