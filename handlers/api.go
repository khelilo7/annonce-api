package api

import (
	"net/http"
	"time"

	"github.com/gin-contrib/cache"
	"github.com/gin-contrib/cache/persistence"
	"github.com/gin-gonic/gin"

	"annonce/handlers/annonce"
)

type App struct {
	Router *gin.Engine
}

func healthCheck(c *gin.Context) {
	c.Header("Content-Type", "text/plain")
	c.String(http.StatusOK, "Healthy")
}

func (app *App) Init() error {
	store := persistence.NewInMemoryStore(60 * time.Second)

	app.Router = gin.Default()
	app.Router.GET("/", healthCheck)
	app.Router.GET("/annonce", annonce.GetAnnonceList)
	app.Router.GET("/annonce/:id", cache.CachePage(store, time.Minute, annonce.GetAnnonce))
	app.Router.POST("/annonce", annonce.CreateAnnonce)
	app.Router.DELETE("/annonce/:id", annonce.DeleteAnnonce)
	return nil
}
